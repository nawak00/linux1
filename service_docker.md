# Intégration d'un container sur un host

## Gestion du réseaux

Lorsqu'un container est lancé il est possible de spécifier dans les options comment il interragi sur le réseaux.

l'option `-p` permet d'ouvrir un port au niveau du docker-host et de transmettre les connexions aux container:

```bash
[vagrant@localhost tmp]$ docker run -p 80:8080/tcp mine/nginxproxy
```

Ici, les connexions sur le docker host (votre machine qui porte le damon docker) sur le port 80, seront retransmise au port 'interne' 8080 du container ainsi instancié.

C'est le port interne, le 8080, que l'on déclare avec l'option expose du dockerfile afin de préciser celui-ci (même si EXPOSE n'est pas du tout indispensable dans le dockerfile).

Ceci c'est pour la partie utilisation en utilisant le driver réseaux par défaut (bridge), mais il existent pas mal d'autres options sur la configuration réseaux des containers que je n'aborderais pas ici faute de temps. Allez voir la [doc docker network](https://docs.docker.com/network/) il y a des tuto sympa.

> utilsez le dépot git@gitlab.com:alsim/flask-hello-from-docker.git pour construire et instancier un container.
> Vous étudierez le dockerfile
> Vous exposerez le port tcp 80 du docker host pour le service réseaux du port 5000 sur le container.
> Testez la connexion sur le port 80 du docker host avec un navigateur web, et rafraîchisser la page une fois ou deux fois.
> Consultez les logs du container.
> Identifiez le processus "python flash" depuis le docker-host et transmetez lui un signal `SIGINT`.
> Consultez les logs à nouveau, quel est l'état du container ?

## Gestion des volumes

Il est possible de monter une sous arborescance du docker host à l'intérieur d'un container, pour ce faire, on utilise l'option `-v`

```bash
[vagrant@localhost tmp]$ docker run -v /opt/data:/data mine/mysql
```

Ici on viens monter la sous arbo /opt/data du docker-host sur le point de montage /data situé à l'intérieur du container. Cela permet au container de travailler sur un dossier du docker-host. Les fichier n'existent qu'une seul fois sur le système (c'est du bind mount).

L'option de build VOLUME du dockerfile permet de déclarer que cette image souhaite disposer d'un volume de données rémanentes. si celle-ci n'est pas monté au run par l'option -v le daemoin docker se chargera de généré ce volume à la volée dans /var/lib/docker/volumes/$un-hash$/_data/. (c'est alors un volume docker)

Encore une fois il existe beaucoup d'options que je ne peu pas détailler maintenant. Je vous encourage à aller voir la [doc officiel docker sur ce sujet](https://docs.docker.com/storage/)

> Utilisez le dépot git@gitlab.com:alsim/reader.git pour construire et instancier un container
> Vous étudierez le Dockerfile
> Vous lancerez ce container afin de monter le dossier /opt/folder du docker_host sur le point de montage /tmp/data dans le container.
> Vous créez deux ou trois fichier dans le dossier /opt/folder sufixé ".todo", exemple /opt/folder/fichier1.todo
> Après quelques une disaine de secondes vérifiez le nom de ce fichier a bien été renommé.
> consultez les logs du container
