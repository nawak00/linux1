# TD-dns

Utilisez vagrant et le dépot git : https://gitlab.com/alsim/tp-dns

![](./images/td-dns.png)

Nous construisons via le vagrantfile :
 
- Deux serveur DNS de récursion accessibles que depuis le réseaux interne (192.168.33.0/24)
- Deux serveurs autoritatifs avec une interface web d’administration, accessible sur le reseaux interne. Le service dns exposer sur le réseaux publique (192.168.56.0/24)
- Le domaine lab.local est povisionné sur les serveurs dns authoritatifs et les serveurs récursifs `forward` les requêtes pour ce domaine vers les serveurs autoritatifs.
- Un reverse proxy accessible depuis le réseaux publique (192.168.56.0/24)
- deux serveurs web wiki et back accessible que depuis le réseaux privé (192.168.33.0/24)

Une interface web est disponible sur les deux serveurs autoritatif via les urls :  http://192.168.33.31/poweradmin/ et http://192.168.33.32/poweradmin/
Login : admin ; mot de passe xxxxxx

> attention après avoir récupèrer le dépot, une seule commande à passer "vagrant up" mais les traitements peuvent être long, ne les interrompez pas.


## Questions

- Connectez vous au host "proxy" avec vagrant et vérifier Comment est configuré le resolver dns du système ?

- Retrouvez l'adresse ip du host `wiki.lab.local` avec la commande dig.

- Connectez vous au host auth-1, quels sont les services réseaux qui sont en fonctionnement actuellement quels sont leur socket d'écoute ?

- Connectez vous au host recursor-1,  quels sont les services réseaux qui sont en fonctionnement actuellement quels sont leur socket d'écoute ?

- Où sont configuré chacuns de ces composants ?

- Qu'est ce qui est configuré sur les serveurs recursifs pour le domaine lab.local ? (Important pour les Actions qui suivent)

- Comment mettre en évidence le fait que le récurseurs ne répondent que sur l’interface du réseaux back (192.168.33.0/24) ?

- Comment est sécurisé l’accès à mysql ?

## Actions

- Ajoutez un nouveau domaine "preprod.local" sur les serveurs autoritatifs interrogeable au travers des serveurs recursifs, décrivez vos actions.

- Mettez en place une config reverse-proxy pour les interfaces poweradmin. Vous préciserez vos actions et dans la configuration reverse proxy le socket d'écoute, le serveur name et la route web configuré.

- Mettez en place une sauvegarde des données dns.

- Mettez en place un second site wiki hébergé de la même manière (sur wiki et back) mais pour le domaine preprod.local

> Pour cette dernière question, après avoir testé vos actions manuelement sur l'un des host, vous créer un nouveau script de provisioning limité au déploiement de cette nouvelle application. Pour tester, vous pouvez faire un wget http://wiki.preprod.local/who.txt (ce fichier devra être généré lors de l'install)
