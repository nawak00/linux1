# Molecule

## Presentation

Molecule est un outils simplifiant le dévelopement de rôle ansible.

Cet outil implante dans la structure arborescente d'un rôle ansible une sous arborescance molecule dédiée à la validation et au test de celui-ci.

> Check the [doc](https://molecule.readthedocs.io/en/stable/)

## Definitions

* role : le role ansible
* driver : c'est le provider d'instance de test (docker, vagrant, LXC, LXD, Openstack, Linode, azure, digital ocean, EC2 etc...) aujourd'hui la version 3 de molécule ne supporte que docker
* instance : un node au sens Ansible, un "host"
* platform : un ensemble cohérent d'instances
* scenario : un scenario est un plan de test complet définissant une plateforme avec un driver et une séquence d'opérations (c'est un sous dossier du dossier molecule)

## quick start

Aller hop, on teste ça.

> ceci est largement inspiré de cette [doc](https://molecule.readthedocs.io/en/stable/getting-started.html)

On créer un nouveau rôle :

```bash
$ molecule init role my-new-role
--> Initializing new role my-new-role...
Initialized role in /home/alan/my-new-role successfully.
$ tree /home/alan/my-new-role/
/home/alan/my-new-role/
├── defaults
│   └── main.yml
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── molecule
│   └── default
│       ├── Dockerfile.j2
│       ├── INSTALL.rst
│       ├── molecule.yml
│       ├── playbook.yml
│       └── tests
│           ├── __pycache__
│           │   └── test_default.cpython-35.pyc
│           └── test_default.py
├── README.md
├── tasks
│   └── main.yml
└── vars
    └── main.yml

9 directories, 12 files
```

Molecule à créer un role avec les main.yml et les dossiers defaults, handlers, meta, tasks et vars ansi qu'une arborescance molecule.
Dans celle-ci, chaque dossier est un scenario de test, le fichier `molecule.yml` défini ce scenario, et le fichier playbook.yml le playbook (utilisant ce role) qui sera testé. (il est nommé converge à partir de la version 3)

```bash
~ $ cd /home/alan/my-new-role
```

On ajoute simplement une tache debug au nouveau role :

```bash
~ $ vi tasks/main.yml
~ $ cat tasks/main.yml
---
# tasks file for my-new-role
- name: Hi there
  debug:
    msg: Hi, there! seems it's works
```

puis on lance les tests :

```bash
~ $ molecule test
.../...
```

Nous avons des erreurs pour le linter 'Ansible lint'

```bash
    [701] Role info should contain platforms
    [703] Should change default metadata: author
    [703] Should change default metadata: description
    [703] Should change default metadata: company
    [703] Should change default metadata: license
```

Nous corrigons simplement en éditant le fichier meta/main.yml :

```yaml
$ grep -v "^ *#" meta/main.yml | grep .
---
galaxy_info:
  author: Alan SIMON
  description: hi there role
  company: i'am with the devil
  license: GPLv3
  min_ansible_version: 1.2
  platforms:
  - name: centos
    versions:
    - all
  - name: debian
    versions:
    - all
  galaxy_tags: []
dependencies: []
```

Si on reteste :

```bash
$ molecule test
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix

└── default
    ├── lint
    ├── dependency
    ├── cleanup
    ├── destroy
    ├── syntax
    ├── create
    ├── prepare
    ├── converge
    ├── idempotence
    ├── side_effect
    ├── verify
    ├── cleanup
    └── destroy
```

Le scenario default et sa séquence d'opérations

* lint : validation syntaxique et de la forme du code de l'ensemble de la configuration molecule et du role.
* dependency : gestion des dépendance (avec galaxy ou glint)
* cleanup : exécution d'un playbook de "netoyage" notament utiliser pour netoyer des ressources externe à la plateforme de test
* destroy : destruction de l plateforme de test
* create : création de la plateforme
* prepare : run d'un playbook gérant des pré-requis attendu par le role (gestion de l'inventaire et des facts par exemple)
* converge : run du playbook de déploiement
* idempotence : second run avec vérification qu'aucune task n'est à l'état "changed"
* side_effect : run d'un playbook générant des évènement extérieur à la plateforme (exemple test de failover, feature expérimental)
* verify : run de tests sur la plateforme après le déploiement. Il conviens de tester ici le fonctionnel, pas la bonne exécution du playbook. plusieur module de test existent.

Les linter yaml, et ansible Lint pour le role puis Flake8 pour les tests :

```bash
--> Scenario: 'default'
--> Action: 'lint'
--> Executing Yamllint on files found in /home/alan/my-new-role/...
Lint completed successfully.
--> Executing Flake8 on files found in /home/alan/my-new-role/molecule/default/tests/...
Lint completed successfully.
--> Executing Ansible Lint on /home/alan/my-new-role/molecule/default/playbook.yml...
Lint completed successfully.
```

(en version 3 de molecule il n'y a plus qu'une seul execution de linter globalement)

Préparation de l'environnement : dependence, netoyage et destruction éventuel d'une plateforme déja existante :

Molecule peu récupèrer des éventueles dépendances (étape dependancy)  [doc molecule sur les dépendances](https://molecule.readthedocs.io/en/latest/configuration.html#dependency) par défaut avec galaxy mais il peu aussi utilise [gitl](https://gilt.readthedocs.io/en/latest/) (un outil de gestion de dépendance git)

```bash
--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
```

Possible exécution d'un playbook cleanup.yml de netoyage :

```bash
--> Scenario: 'default'
--> Action: 'cleanup'
Skipping, cleanup playbook not configured.
```

Destruction de plateforme si existante :

```bash
--> Scenario: 'default'
--> Action: 'destroy'
--> Sanity checks: 'docker'

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=instance)

    TASK [Wait for instance(s) deletion to complete] *******************************
    ok: [localhost] => (item=None)
    ok: [localhost]

    TASK [Delete docker network(s)] ************************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
```

Puis la création de l'environnement **avec Ansible !** (inception):

```bash
--> Scenario: 'default'
--> Action: 'syntax'
--> Sanity checks: 'docker'

    playbook: /home/alan/my-new-role/molecule/default/playbook.yml
--> Scenario: 'default'
--> Action: 'create'

    PLAY [Create] ******************************************************************

    TASK [Log into a Docker registry] **********************************************
    skipping: [localhost] => (item=None)

    TASK [Create Dockerfiles from image names] *************************************
    changed: [localhost] => (item=None)
    changed: [localhost]

    TASK [Determine which docker image info module to use] *************************
    ok: [localhost]

    TASK [Discover local Docker images] ********************************************
    ok: [localhost] => (item=None)
    ok: [localhost]

    TASK [Build an Ansible compatible image (new)] *********************************
    ok: [localhost] => (item=molecule_local/centos:7)

    TASK [Build an Ansible compatible image (old)] *********************************
    skipping: [localhost] => (item=molecule_local/centos:7)

    TASK [Create docker network(s)] ************************************************

    TASK [Determine the CMD directives] ********************************************
    ok: [localhost] => (item=None)
    ok: [localhost]

    TASK [Create molecule instance(s)] *********************************************
    changed: [localhost] => (item=instance)

    TASK [Wait for instance(s) creation to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) creation to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]

    PLAY RECAP *********************************************************************
    localhost                  : ok=7    changed=3    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0
```

Puis préparation (playbook prepare) et exécution du playbook d'application du role (playbook.yml ou converge.yml en version 3):

```bash
--> Scenario: 'default'
--> Action: 'prepare'
Skipping, prepare playbook not configured.
--> Scenario: 'default'
--> Action: 'converge'

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [instance]

    TASK [my-new-role : Hi there] **************************************************
    ok: [instance] => {
        "msg": "Hi, there! seems it's works"
    }

    PLAY RECAP *********************************************************************
    instance                   : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

Validation de l'idempotence, les sides effects et le test du résultat :

```bash
--> Scenario: 'default'
--> Action: 'idempotence'
Idempotence completed successfully.
--> Scenario: 'default'
--> Action: 'side_effect'
Skipping, side effect playbook not configured.
--> Scenario: 'default'
--> Action: 'verify'
--> Executing Testinfra tests found in /home/alan/my-new-role/molecule/default/tests/...
    ============================= test session starts ==============================
    platform linux -- Python 3.5.2, pytest-5.2.2, py-1.8.0, pluggy-0.13.0
    rootdir: /home/alan/my-new-role/molecule/default
    plugins: testinfra-3.2.0
collected 1 item


    tests/test_default.py .                                                  [100%]

    ============================== 1 passed in 3.30s ===============================
Verifier completed successfully.
```

et après les tests, le netoyage et la destruction de la plateforme de test :

```bash
--> Scenario: 'default'
--> Action: 'cleanup'
Skipping, cleanup playbook not configured.
--> Scenario: 'default'
--> Action: 'destroy'

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=instance)

    TASK [Wait for instance(s) deletion to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]

    TASK [Delete docker network(s)] ************************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

--> Pruning extra files from scenario ephemeral directory
```

## Configuration

le dossier scénarios de molécule contiendra les playbooks : playbook.yml(converge.yml), prepare.yml, clean.yml, et side_effect.yml ; le fichier requirement.yml ; et les tests implémentés.

le fichier molecule.yml permet de confgiurer tout ça :

v2 : (en v2 on a linter par composant)

```yaml
---
dependency:
  name: galaxy
driver:
  name: docker
lint:
  name: yamllint
platforms:
  - name: instance
    image: centos:7
provisioner:
  name: ansible
  lint:
    name: ansible-lint
verifier:
  name: testinfra
  lint:
    name: flake8
```

v3 :

```yaml
---
dependency:
  name: galaxy
driver:
  name: docker
lint: |
  set -e
  yamllint .
  ansible-lint
  flake8
platforms:
  - name: instance
    image: centos:7
provisioner:
  name: ansible
verifier:
  name: testinfra
```

On notera ici :

* la gestion des dépendances avec galaxy (si le requirement.yml est présent)
* le driver d'instances : docker
* le Linter globale (les commandes de lint)
* la description de plateforme : une seul instance centos7 nommé instance
* le provisioner ansible
* et enfin le verifier, ici on utilise `testinfra` la bibliothèque de test d'infra python (voir le fichier molecule/default/tests/test_default.py) et la [doc](https://testinfra.readthedocs.io/en/latest/). il est aussi possible d'utiliser un playbook de validation (verify.yml) [doc verifier](https://molecule.readthedocs.io/en/latest/configuration.html#id13)

> [doc configuration molecule](https://molecule.readthedocs.io/en/stable/configuration.html)

## Usage

Nous pouvons utiliser les scénarios complet molecule pour dérouler des tests complets mais nous pouvons aussi simplement pendant le dev, lancer une des partie du scénrario :

on créé l'intance :

```bash
$ molecule create
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix

└── default
    ├── dependency
    ├── create
    └── prepare

.../...
```

On dispose ainsi d'un environement pour le dev consultable en interactif avec `molecule login [nom-instance]`:

```bash
$ molecule login
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
[root@instance /]# exit
$
```

Jouer son role dessus (converge)

```bash
$ molecule converge
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix

└── default
    ├── dependency
    ├── create
    ├── prepare
    └── converge

--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
--> Scenario: 'default'
--> Action: 'create'
Skipping, instances already created.
--> Scenario: 'default'
--> Action: 'prepare'
Skipping, prepare playbook not configured.
--> Scenario: 'default'
--> Action: 'converge'

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [instance]

    TASK [my-new-role : Hi there] **************************************************
    ok: [instance] => {
        "msg": "Hi, there! seems it's works"
    }

    PLAY RECAP *********************************************************************
    instance                   : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

Repetez les deux actions précentes pendant tout le dèv (login pour débug et re-déploiement du role)

ensuite on validera l'idempotence :

```bash
$ molecule idempotence
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix

└── default
    └── idempotence

--> Scenario: 'default'
--> Action: 'idempotence'
Idempotence completed successfully.
```

et enfin détruire l'environement

```bash
$ molecule destroy
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix

└── default
    ├── dependency
    ├── cleanup
    └── destroy

--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
--> Scenario: 'default'
--> Action: 'cleanup'
Skipping, cleanup playbook not configured.
--> Scenario: 'default'
--> Action: 'destroy'

    PLAY [Destroy] *****************************************************************

    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=instance)

    TASK [Wait for instance(s) deletion to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]

    TASK [Delete docker network(s)] ************************************************

    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

--> Pruning extra files from scenario ephemeral directory
```

## Tuneable

En configurant le fichier molecule.yml on pourra définir précisément son environement de test (les instances, les images docker) et ce que l'on souhaite tester :
Il sera alors possible :

* de créer/modifier les playbooks playbook.yml(converge.yml), prepare.yml, clean.yml, et side_effect.yml ;  
* gèrer les dépendances galaxy avec un fichier requirement.yml ;
* et implémenter ses tests (dossier tests/ avec testinfra ou un playbook verifier.yml)

Il est part ailleurs aussi possible de créer d'autre scenarios de test (init scenario -r role -s nom-scenrario -d driver), basé sur un driver vagrant par exemple (*en version 2 uniquement*):

```bash
$ molecule init scenario -r my-new-role -d vagrant -s vagrant
--> Initializing new scenario vagrant...
Initialized scenario in /home/alan/my-new-role/molecule/vagrant successfully.
$ tree molecule/vagrant/
molecule/vagrant/
├── INSTALL.rst
├── molecule.yml
├── playbook.yml
├── prepare.yml
└── tests
    ├── __pycache__
    │   └── test_default.cpython-35.pyc
    └── test_default.py
```

Modifié pour utiliser une box centos/7 car elle existe déja en locale.

```yaml
molecule/vagrant/molecule.yml:
---
dependency:
  name: galaxy
driver:
  name: vagrant
  provider:
    name: virtualbox
lint:
  name: yamllint
platforms:
  - name: instance
    box: centos/7
provisioner:
  name: ansible
  lint:
    name: ansible-lint
verifier:
  name: testinfra
  lint:
    name: flake8
```

```bash
$ molecule create -s vagrant
--> Validating schema /home/alan/my-new-role/molecule/vagrant/molecule.yml.
Validation completed successfully.
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix

└── vagrant
    ├── dependency
    ├── create
    └── prepare

```

## conclusion

Voilà de quoi s'amuser a développer des role ansible réutilisable comme une boite à outil pour sysadmin. il sont alors testés et on applique les bonnes pratiques.

__A faire (Un role testé):__

implémentez un role choisi parmis les propositions suivantes, ou une autre si vous avez des idées. Il n'est pas interdit d'utiliser un role déja implémenté sur galaxy et d'en faire une meilleur version :

un role simple mais applicable et testé sur plusieurs distributions : debian 8 9 10 et/ou centos 7 et 8 :

* utilisateur : gèrer la création et les droits des équipes admin et support sur l'ensemble de l'infra (dépot de clefs ssh et droits sudo pour certain) il faudra bien définir ou sont les clefs publique des utilisateur
* secure-ssh : configurer ssh en mode secure, pour interdire les logins root et par mot de passe, le forward d'agent, le port forwarding etc....  (ou en suivant cette [doc](https://cryptsus.com/blog/how-to-secure-your-ssh-server-with-public-key-elliptic-curve-ed25519-crypto.html))
* nginx : déployer nginx et des config reverse proxy et virtualhosts
* apache : déployer apache et des virtualhosts
* php-fpm : déployer et configurer php-fpm
* nginx et uwsgi : pour déployer des applis web python
* mysql : deployer mysql et créer des bases mysqls, gérer la sauvegarde.... et pourquoi pas la mise en oeuvre d'un master slave
* webapp : pour simplement déployer des appli web utilisant les roles précédents.

ou plus complexe pouvant se limiter à une seul distribution:

* docker-compose : qui installe docker et docker-compose et qui permet de livrer et lancer des projets docker-compose (ça risque d'être difficile à tester en environnement docker en vagrant c'est plus simple donc molecule version 2.22)
