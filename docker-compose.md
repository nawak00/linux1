# Docker compose

## Présentation

Nous avons vu la construction d'une image et le lancement simple de container. Cependant les applications fonctionnent en s'appuyant sur plusieurs briques logiciels interconnectés : serveur web, moteur de langage, base de donnée, etc...

docker-compose est un outil permettant de géré un système multicontainer. Il permet de synthétiser dans un fichier Yaml, donc facimement lisible les paramètres de construction et d'instanciation de l'ensemble des containers concernés.

## Installation

Encore une fois, on se réfère à la [documentation officiel](https://docs.docker.com/compose/install/) pour l'installation, et pas un vieux tuto (hein Florian?)

**Si vous ré-utilisé le vagrantfile proposé [ici](./utilisation_docker.md#Environnement) (récupèrez le à nouveau) et vous tappez depuis votre environement vagrant vagrant provision provision --provision-with compose**

## Le YAML

Le YAML : **Y**et **A**nother **M**eta **L**anguage , c'est un langage descriptif. Les structures de bases du YAML sont :

* *liste* : une suite d'entrée **de même indentation** préfixées par un `-`

```yaml
- élément1
- élément2
- élément3
```

* *dictionaire* (*dict*) : un enchaînement de `clef: valeur`
  * le séparateur est la suite : **deux points suivis par un espace**
  * la valeur peut être une simple valeur, une liste ou un autre *dictionnaire*

  ```yaml
  - player1:
      name: rick
      friends:
          - morty
          - bart
  - player2:
      name: morty
      friends:
          - bart
  - player3:
      name: bart
      friends:
          - bart
  ```

  Nous avons ici une liste de trois *dictionnaires* `player1`, `player2` et `player3` dont les valeurs sont des *dictionnaires* de deux paires de clef/valeur : `name` et `friends`. La clef `friends` à pour valeur une autre *liste*...

Vous noterez que l'indentation est **trèèès importante**. Elle est en fait essentielle à un formattage YAML correct. Le fichier sera incorrect si l'indentation est incorrecte.

> Le YAML est en vérité un peu plus complexe et riche que ça mais cela est sufisant pour compose.

## Docker-compose

On viens placer le fichier docker-compose.yml dans l'arborescence projet afin de simplement automatiser les opérations docker :

* gestion des images : le build si neccessaire
* gestion des containers : les instanciation, les arrêts v/ redémarrage
* gestion des volumes : création des volumes (nous utiliserons des named volume plutot que du bind pour les données)
* gestion des réseaux : création des bridges réseaux

### L'environnement testé

Voici l'arborescence à reproduire :

```bash
[vagrant@localhost compose]$ tree .
.
├── docker-compose.yml
└── gitea
    ├── app.ini
    └── Dockerfile
```

J'ai utilisé l'image gitea légèrement modifier :

```docker
FROM alpine
ENV GITEA_WORK_DIR=/opt/gitea/var
ENTRYPOINT ["/opt/gitea/gitea", "web"]
EXPOSE 3000
EXPOSE 22
RUN apk add wget git openssh-keygen bash && \
    addgroup -S -g 102 git && \
    mkdir -p /opt/gitea/var/data && \
    mkdir -p /opt/gitea/var/custom/conf && \
    chown -R 102:102 /opt/gitea && \
    adduser -s /bin/bash -u 102 -h /opt/gitea/var/data -S -G git -D git
ADD app.ini /opt/gitea/var/custom/conf/
RUN wget -O /opt/gitea/gitea https://dl.gitea.io/gitea/1.11.3/gitea-1.11.3-linux-amd64 && chmod 755 /opt/gitea/gitea
USER git
VOLUME /opt/gitea/var
```

J'ai ajouté dans l'image le fichier **app.ini** contenant la configuration gitea que j'ai confectionné par ailleurs (à partir d'une nouvelle instance que j'ai configuré via l'interface, je récupère enssuite le fichier app.ini)

<details>
<summary>  app.ini :</summary>

```ini
APP_NAME = it s mine
RUN_USER = git
RUN_MODE = prod

[oauth2]
JWT_SECRET = Lx8E6L_SX4gbZkzehWLxgbpXwnVHMoc4e7p0uqznBiI

[security]
INTERNAL_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1ODQ3NDQ0OTN9.f3SQOfVKocJklnSMIXRO5UYoAAiAlgPJHJwNbZn5Y34
INSTALL_LOCK   = true
SECRET_KEY     = cSjKKL5dtwzNg9OBQXMeLbZZSqBytFyq3Jy5sGMRH4AP2l9Ut6yTE10jyNn0CGyI

[database]
DB_TYPE  = mysql
HOST     = db:3306
NAME     = gitea
USER     = gitea
PASSWD   = gitea
SSL_MODE = disable
CHARSET  = utf8

[repository]
ROOT = /opt/gitea/var/data/gitea-repositories

[server]
SSH_DOMAIN       = 192.168.33.5
DOMAIN           = 192.168.33.5
HTTP_PORT        = 3000
ROOT_URL         = http://192.168.33.5/
DISABLE_SSH      = false
START_SSH_SERVER = true
SSH_PORT         = 2222
LFS_START_SERVER = true
LFS_CONTENT_PATH = /opt/gitea/var/data/lfs
LFS_JWT_SECRET   = rjyYcStOiSw4e7hxjh8Jq1szQtUiLqbdceB9VH-Egtk
OFFLINE_MODE     = false

[mailer]
ENABLED = false

[service]
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = false
DISABLE_REGISTRATION              = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.localhost

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = info
ROOT_PATH = /opt/gitea/var/log
```

</details>

### Le fichier docker-compose.yml

On défini les deux images utilisée, leur réseaux et leurs volumes:

```yaml
version: "2"           # version de compose

networks:              # Je défini les réseaux pour cette infra
  internal-net:
  access-net:
    driver: bridge     # Je précise bridge mais c'est en fait déja la valeure par défaut... just pour dire que l'on peu changer de driver

volumes:               # Je défini les volumes ici
  data-mysql:
    driver: local      # j'utilise des volumes locaux, simple
  gitea-data:
    driver: local

services:              # chaque service est en fait ujn container
  gitea:
    build: "./gitea"   # le dossier de build est le dossier gitea dans le dossier contenant le docker compose
    restart: always    # en cas de crash le container sera redémarré
    networks:          # les réseaux pour ce container
      - access-net
      - internal-net
    volumes:           # les volumes, il est possible de partager des fichiers de config do docker host vers le container
      - gitea-data:/opt/gitea/var
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:             # les service qui seront exposé
      - "80:3000"
      - "2222:2222"
    depends_on:        # ce service nécessite le service db présenté plus bas
      - db

  db:
    image: mysql:5.7   # j'utilise une image toute faite
    restart: always    #
    environment:       # cette image utilise des variables d'environement pour s'auto set-up
      - MYSQL_ROOT_PASSWORD=gitea
      - MYSQL_USER=gitea
      - MYSQL_PASSWORD=gitea
      - MYSQL_DATABASE=gitea
    networks:          # uniquement le réseaux interne
      - internal-net
    volumes:
      - data-mysql:/var/lib/mysql
```

### Les commandes

`docker-compose --help` pour l'aide puis `docker-compose $action --help` pour l'aide dédié à l'action $action.

`docker-compose build` : Pour reconstruire les images disposants de spécification de build.

`docker-compose up -d`: Cette command passée depuis le dossier contenant récupère le fichier docker-compose et :

* Créé les volumes
* Créé les réseaux
* Construira l'image
* instancie les containers (-d pour --detach)

`docker-compose stop` : Pour arrêté l'ensemble des container

`docker-compose down` : Pour supprimer container et configuration réseaux (et le reste des objets créé)

## fin de la config de gitea

une fois démarré gitea initialise sa base de donnée cependant sans aucun compte.

Afin de créer un premier compte admin, on se connecte au container gitea avec un shell afin d'utiliser la commande `gitea admin`  (voir la [doc](https://docs.gitea.io/en-us/command-line/) sur la cli gitea)

```bash
[vagrant@localhost ~]$ docker exec -it compose_gitea_1 bash
bash-5.0$ /opt/gitea/gitea admin create-user --admin --username alan --password xxxxxx --email me@example.com
.../...
New user 'alan' has been successfully created!
bash-5.0$ exit
[vagrant@localhost ~]$
```

## Les éléments virtuel docker géré par compose

## Les volumes docker

S'il n'est pas bindé sur l'arborescance, le volume est géré par docker dans une arboresance dédié /var/lib/docker/volumes. Ceci est directement géré par docker, compose nomme les volumes automatiquement.

```bash
[vagrant@localhost compose]$ docker volume ls
DRIVER              VOLUME NAME
local               b2e9a7247cf06261cdfb95d5503c0c8be84d92ae8d6dca5b948a22aeba0d6852
local               compose_data-mysql
local               compose_gitea-data
[vagrant@localhost compose]$ sudo ls /var/lib/docker/volumes/
b2e9a7247cf06261cdfb95d5503c0c8be84d92ae8d6dca5b948a22aeba0d6852
compose_data-mysql
compose_gitea-data
metadata.db
```

Le volume b2e9a7247cf06261cdfb95d... lui n'a pas été nommé (c'est le volume qui à été créer lorsque j'ai testé le dockerfile contenant la clause VOLUME), les autre ont été nommé par compose.

## les réseaux docker

### Les bridges linux

Les bridges sont des switch virtuels, ils permettent de connecter les interface réseaux virtuels des containers

`brctl` est une commande permettant de consulter les bridges linux:

```bash
[vagrant@localhost compose]$ brctl show
bridge name       bridge id           STP enabled     interfaces
br-810537c4bdd1   8000.0242f066a780   no              veth5ebe338
br-8e01e71bcac5   8000.0242725bbf53   no              veth290eb5a
                                                      veth74be668
docker0           8000.02420bc80884   no
```

Avec docker network on retrouve les bridge ansi créé et les interface virtuel attaché aux container.

```bash
[vagrant@localhost compose]$ docker network ls
NETWORK ID          NAME                   DRIVER              SCOPE
60b5870d5583        bridge                 bridge              local
810537c4bdd1        compose_access-net     bridge              local
8e01e71bcac5        compose_internal-net   bridge              local
234dd9405952        host                   host                local
32a18eea0cb1        none                   null                local
```

En déclarant des réseaux dans compose, des bridges sont créé à la volé au "up". à l'usage de docker comme vue dans les étapes précédentes en l'absence d'action suplémentaire, tous les container utilisent le même bridge : docker0.

Allez voir la [doc docker network](https://docs.docker.com/network/) il y a des tuto sympa en bas présentant les autres options de configuration réseaux.
