# Ressources et Discussion

## Les bonnes pratiques Ansible

Consultez les [bonne pratiques ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)

Discussion :

* comment gèrer proprement les environements : Lab / preprod / prod ? plusieurs solutions..

## Tester ses playbook

Consultez la doc sur [les stratégies de test Ansible](https://docs.ansible.com/ansible/latest/reference_appendices/test_strategies.html)

Discussion :

* Comment aplliquer les même playbooks sur tout les environements : lab, stagging, prod ?

## Organisation du code ansible

Nous avons vu:

* Que les roles peuvent être complètetement indépendant du reste du code, ils peuvent donc être géré dans leur propre dépots.
* l'inventaire instancie les configurations en précisant les valeure des variables.

A voir :

* le fichier site.yml viens lister les playbooks devant être appliqué sur l'infra.
* le fichier requirement.yml qui définis les composant ansible nécessaire (roles)

Certaines organisations utilisent un seul dépot git contenant l'ensemble du code, l'inventaire, les roles, les playbook. En revanche il est possible d'avoir une aproche plus modulaire, et donc de coder, maintenir les composants, indépendament. c'est ma quête actuelle.

## Gestions des secret

A voir: [ansible-vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html)

Cette commande permet de créer, modifier, consulter des fichiers de variables secrettes (chiffrées). A chacune des utilisations de ces fichiers de variables la clef de chiffrement sera demandé. Il est donc pas complètement interdit de les stocker dans un dépot git (à accès restreint quand même).

## Inventaire dynamique

Plutot que de gérer votre inventaire de hosts et ses variables manuelement, il est possible d'utiliser une application proposant une api qui serais consulté par ansible comme inventaire. (l'application netbox fais ça)
