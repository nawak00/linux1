# Construire une image docker fonctionnelle

## Sujet

Construire une image portant le server git gitea sans bien sur utiliser l'installation via docker de la doc officielle.

gittea sera paramètré pour utiliser sqllite et l'appli fonctionnera donc dans un container autonome.

Le run devra intégré la redirection de ports et le bind mount d'un FS du docker host vers le container afin que les données soient rémanente sur le docker host.

> Après l'initialisation de l'appli, vous testerez la suppression du container et l'instanciation d'un nouveau afin de vérifier qu'il réutilise la config laissé par le précédent.

## Correction

<details>
<summary>lire uniquement après avoir fais l'exo!</summary>


Voici le dockerfile que j'ai confectionné :

```docker
FROM alpine
ENV GITEA_WORK_DIR=/opt/gitea/var
ENTRYPOINT ["/opt/gitea/gitea", "web"]
EXPOSE 3000
EXPOSE 22
RUN apk add wget sqlite git openssh-keygen bash && \
    addgroup -S -g 102 git && \
    mkdir -p /opt/gitea/var/data && \
    adduser -s /bin/bash -u 102 -h /opt/gitea/var/data -S -G git -D git && \
    chown -R git:git /opt/gitea/
RUN wget -O /opt/gitea/gitea https://dl.gitea.io/gitea/1.11.3/gitea-1.11.3-linux-amd64 && chmod 755 /opt/gitea/gitea
USER git
```

- je part d'une alpine.
- les port 3000 et 22 seront exposé depuis le container
- Un point de montage est nécessaire sur /opt/gitea/var
- une variable d'environement doit être présente GITEA_WORK_DIR=/opt/gitea/var
- la commande lancée dans le container sera `/opt/gitea/gitea web`
- j'install wget sqlite, git et ssh-keygen avec `apk`
- jes créé les user et groupes système **git** sur les uid et gid **102**
- je créer le dossier /opt/gitea aparatenant au compte git
- je récupère l'exécutable et je met les droits d'exécution dessus
- je fix le compte de run au user git

> J'ai parcouru la doc vite fait, puis j'ai juste installer wget et sqlite et récupèrer puis chmod l'exécutable en go. vue le message d'erreur il me manquait git, j'ai rebuild en installant git. j'ai finalement eu un container (après plusieurs essais hein).
> La configuration m'a aussi aporté des éclairsissement sur l'appli
> j'ai ensuite améliorer le dockerfile afin de :
>
> - lancer l'exécutable avec le compte git
> - exposer les port et définir le volume
> - exporté la variable GITEA_WORK_DIR
>  

Voici la commande de build :

```bash
[vagrant@localhost ~]$ docker build -t mine/gitea:latest .
```

Et enfin les commandes de préparration et de run :

```bash
[vagrant@localhost ~]$ sudo mkdir -p /opt/gitea/var
[vagrant@localhost ~]$ sudo chown -R 102:102 /opt/gitea/var
[vagrant@localhost ~]$ docker run -v /opt/gitea/var:/opt/gitea/var -p 80:3000 -p 2222:22 -d mine/gitea
```

- je créer le dossier /opt/gitea/var que je fait aparatenir a l'uid/gid 102
- je lance le container

Configuration gitea :

- on utilisera sqllite en backend
- je change les chemins afin d'utiliser /opt/gitea/var/ pour toutes les données
- je spécifie l'url d'usage (on mettra ici le nom de domaine qui va bien)
- et enfin je me créé un compte

![config1](./images/gitea-config-1.png)

![config2](./images/gitea-config-2.png)

![config3](./images/gitea-config-3.png)

Résultat :

Toutes les données son visible sur le docker host et appartiènent au user/groupe 102:102

```bash
[vagrant@localhost ~]$ sudo ls -al /opt/gitea/var/*
/opt/gitea/var/custom:
total 0
drwxr-xr-x. 3 102 102 18 20 mars  22:48 .
drwxr-xr-x. 5 102 102 43 20 mars  22:49 ..
drwxr-xr-x. 2 102 102 21 20 mars  22:57 conf

/opt/gitea/var/data:
total 1096
drwx------. 9 102 102     187 20 mars  23:00 .
drwxr-xr-x. 5 102 102      43 20 mars  22:49 ..
-rw-------. 1 102 102      38 20 mars  22:51 .ash_history
-rw-------. 1 102 102      15 20 mars  22:56 .bash_history
-rw-r--r--. 1 102 102     123 20 mars  22:57 .gitconfig
drwx------. 2 102 102      29 20 mars  22:55 .ssh
drwxr-xr-x. 3 102 102      18 20 mars  22:50 gitea-repositories
-rw-r--r--. 1 102 102 1110016 20 mars  23:00 gitea.db
drwx------. 3 102 102      26 20 mars  22:49 indexers
drwxr-xr-x. 2 102 102       6 20 mars  22:49 lfs
drwxr-xr-x. 4 102 102      39 20 mars  22:49 queues
drwx------. 3 102 102      15 20 mars  22:53 sessions
drwxr-xr-x. 2 102 102      42 20 mars  22:57 ssh

/opt/gitea/var/log:
total 188
drwxr-xr-x. 2 102 102     23 20 mars  22:49 .
drwxr-xr-x. 5 102 102     43 20 mars  22:49 ..
-rw-r-----. 1 102 102 190206 20 mars  23:00 gitea.log
```

Nous pouvons supprimer et relancer un nouveau container, il retrouve les données présentent ici.

> Après quelques tests de validation (création d'un dépot et tentative de clone) j'ai du mofifier un peu la conf :
> j'ai ajouté `START_SSH_SERVER = true` et modifié `SSH_PORT  = 2222`
> C'est aussi là que j'ai vue que ssh-keygen et bash était nécessaire en scrutant les logs. La doc est donc incomplète sur les pré-requis ou dépendances necessaires au bon fonctionnement.
>
> Pour info, j'ai tappé plus de 375 commandes pour réaliser ceci avec les essais / réessais et ce sans compter les commandes passé à l'intérieur des container pour débug (ce que j'ai fais 14 fois). J'ai juste regarder mon historique sur le docker_host pour vous founir ces détails. J'y ai passé plus de 2 heures pleines (en comptant la rédaction de ce document)

Il reste pas mal de choses à faire...
exemple : faire des sauvegarde : [doc backuo/restor gitea](https://docs.gitea.io/en-us/backup-and-restore/)

</details>

Posez moi donc des questions si vous voullez en savoir plus...
